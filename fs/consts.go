package fs

import "regexp"

const TV_SHOWS_FOLDER = "TvShows"
const MOVIES_FOLDER = "Movies"

var MEDIA_FILES_REGEX = regexp.MustCompile("(?i).+\\.(mkv|avi|mp4)$")
var TV_SHOW_REGEX = regexp.MustCompile(`(?im)^(.+)((s[0-9]{2}e[0-9]{2})|([0-9]{2}x[0-9]{2}))(.+)(\.mkv|avi|mp4)$`)
var TV_SHOW_SEASON_REGEX = regexp.MustCompile(`(?im)((s)([0-9]{2})(e)([0-9]{2}))|(([0-9]{2})(x)([0-9]{2}))`)
var MOVIE_REGEX = regexp.MustCompile(`(?im)^(.+)([\._\-\s][1-2][0-9][0-9][0-9][\._\-\s])(.+)(\.mkv|avi|mp4)$`)
var YEAR_REGEX = regexp.MustCompile(`(?im)([\._\-\s][1-2][0-9][0-9][0-9][\._\-\s])`)
var DELIMITER_REGEX = regexp.MustCompile(`(?m)[\._-]`)
