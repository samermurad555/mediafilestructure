package fs

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
)

func fileOrFolderExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}

func WalkSource(sourceFolder string, destFolder string) []MediaFile {
	arr := make([]MediaFile, 0)
	err := filepath.Walk(sourceFolder,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				panic(err)
			}
			if path == destFolder {
				return filepath.SkipDir
			}
			if !strings.Contains(destFolder, path) && MEDIA_FILES_REGEX.Match([]byte(path)) {
				mfile := MediaFile{
					AbsoluteDir: path,
					Folder:      filepath.Dir(path),
					FileName:    info.Name(),
					Size:        info.Size(),
					IsTvShow:    TV_SHOW_REGEX.Match([]byte(info.Name())),
				}
				arr = append(arr, mfile)
			}
			return nil
		})
	if err != nil {
		log.Println(err)
	}
	return arr
}

func ReadyFileTransfers(mFiles []MediaFile, destFolder string) []FileTransfer {
	fs := make([]FileTransfer, 0)
	for _, file := range mFiles {
		meta := file.Meta()
		if meta != nil {
			fT := NewFileTransfer(meta, destFolder)
			fs = append(fs, *fT)
		}
	}
	return fs
}

func NewFileTransfer(m *MediaFileMeta, destFolder string) *FileTransfer {
	f := FileTransfer{OldDir: m.Path, Title: m.Title}
	basePath := destFolder
	f.NewDir = filepath.Join(basePath, m.WrappingFolder())
	return &f
}

func TransferFiles(mFiles []FileTransfer) {
	fmt.Println("Moving all files")

	for _, file := range mFiles {
		dir := filepath.Dir(file.NewDir)
		if ok, err := fileOrFolderExists(dir); err == nil && !ok {
			os.MkdirAll(dir, os.ModePerm)
		} else if err != nil {
			panic(err)
		}

		err := os.Rename(file.OldDir, file.NewDir)
		if err != nil {
			panic(err)
		}
	}
}
