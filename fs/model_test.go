package fs

import (
	"fmt"
	"path/filepath"
	"testing"
)

func mkFile(url string) MediaFile {
	dir := filepath.Dir(url)
	base := filepath.Base(url)
	isTV := TV_SHOW_REGEX.Match([]byte(base))
	return MediaFile{url, dir, 0, base, isTV}
}
func Test1(t *testing.T) {
	mf := mkFile("/skamx/Arthur_Christmas_2011_1080p_Arthur_Christmas_2011_1080p.Molpol.mkv")
	fmt.Println(mf.Meta())
	mfm := mf.Meta()
	if mfm == nil {
		t.Errorf("MediaFile Failed to create Meta")
		return
	}

	want := "Movies/Arthur Christmas (2011)/Arthur_Christmas_2011_1080p_Arthur_Christmas_2011_1080p.Molpol.mkv"
	got := mfm.WrappingFolder()
	if want != got {
		t.Errorf("%v != %v", want, got)
	}
}

type AsrtErr string

func (err AsrtErr) Error() string {
	return string(err)
}
func sAssrt(want string, got string) error {
	if got == want {
		return nil
	}
	return AsrtErr(fmt.Sprintf("\n\n\033[0m \033[31mWant: %v \n\n\033[0m \033[32mGot: %v\033[0m \033[0m", want, got))
}
func Test2(t *testing.T) {
	mf := mkFile(`/qeed_12 LP/the.Awesome.Show.2019.s02e14.hnss.3oi9.mkv`)
	if mfm := mf.Meta(); mfm == nil {
		t.Errorf("MediaFile Failed to create Meta")
		return
	} else {
		want := "TvShows/The Awesome Show (2019)/Season 02/the.Awesome.Show.2019.s02e14.hnss.3oi9.mkv"
		got := mfm.WrappingFolder()
		if err := sAssrt(want, got); err != nil {
			t.Errorf(err.Error())
		}
	}
}
