package fs

import (
	"fmt"
	"path/filepath"
	"strings"
)

type MediaFile struct {
	AbsoluteDir string `json:"absoluteDir"`
	Folder      string `json:"folder"`
	Size        int64  `json:"size"`
	FileName    string `json:"fileName"`
	IsTvShow    bool   `json:"isTvShow"`
}

type FileTransfer struct {
	Title  string `json:"title"`
	OldDir string `json:"oldDir"`
	NewDir string `json:"newDir"`
}

type MediaFileMeta struct {
	Path     string `json:"path"`
	IsTvShow bool   `json:"isTvShow"`
	Year     string `json:"year"`
	Season   string `json:"season"`
	Title    string `json:"title"`
}

func (mF MediaFile) Season() string {
	season := ""
	if mF.IsTvShow {
		seasonMatch := TV_SHOW_SEASON_REGEX.FindAllStringSubmatch(mF.FileName, -1)
		if seasonMatch[0][3] != "" {
			season = seasonMatch[0][3]
		} else {
			season = seasonMatch[0][7]
		}
	}
	return season
}

func (mF MediaFile) Year() string {
	year := ""
	yearMatch := YEAR_REGEX.FindAllStringSubmatch(mF.FileName, -1)
	if len(yearMatch) > 0 {
		year = yearMatch[0][1]
		year = string(DELIMITER_REGEX.ReplaceAll([]byte(year), []byte(" ")))
		year = strings.Trim(year, " ")
	}
	return year
}
func (mF MediaFile) Title() string {
	title := ""
	metaFullMatch := make([][]string, 0)
	if mF.IsTvShow {
		metaFullMatch = TV_SHOW_REGEX.FindAllStringSubmatch(mF.FileName, -1)
		if len(metaFullMatch) == 0 {
			return ""
		}
		title = metaFullMatch[0][1]
	} else {
		metaFullMatch = MOVIE_REGEX.FindAllStringSubmatch(mF.FileName, -1)
		if len(metaFullMatch) == 0 {
			return ""
		}
		title = metaFullMatch[0][1]
		indices := YEAR_REGEX.FindIndex([]byte(title))
		if len(indices) > 0 {
			title = title[:indices[0]]
		}
	}

	if title != "" && TV_SHOW_SEASON_REGEX.Match([]byte(title)) {
		arr := TV_SHOW_SEASON_REGEX.FindIndex([]byte(title))
		if len(arr) > 0 {
			title = title[:arr[0]]
		}
	}
	title = string(DELIMITER_REGEX.ReplaceAll([]byte(title), []byte(" ")))
	title = string(YEAR_REGEX.ReplaceAll([]byte(title), []byte("")))

	title = strings.Trim(title, " ")
	return strings.Title(strings.ToLower(title))
}
func (mF MediaFile) Meta() *MediaFileMeta {
	title := mF.Title()
	season := mF.Season()
	year := mF.Year()
	if title == "" {
		return nil
	}
	mft := &MediaFileMeta{
		IsTvShow: mF.IsTvShow,
		Path:     mF.AbsoluteDir,
		Year:     year,
		Season:   season,
		Title:    title,
	}
	return mft
}

func (mfm *MediaFileMeta) WrappingFolder() string {
	wrap := mfm.Title
	if mfm.Year != "" {
		wrap += " (" + mfm.Year + ")"
	}
	if mfm.IsTvShow {
		return filepath.Join(
			TV_SHOWS_FOLDER,
			wrap,
			fmt.Sprintf("Season %v", mfm.Season),
			filepath.Base(mfm.Path),
		)
	} else {
		return filepath.Join(MOVIES_FOLDER, wrap, filepath.Base(mfm.Path))
	}
}
