package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"

	"github.com/google/uuid"
	"www.samermurad.com/mfsp/config"
	"www.samermurad.com/mfsp/fs"
)

var APPROVAL_REG = regexp.MustCompile("(?i)^y$")

func YesNoQuestion(q string) bool {
	fmt.Println(q)
	var input string
	fmt.Scanln(&input)
	return APPROVAL_REG.Match([]byte(input))
}

func SaveTemp(arr []fs.FileTransfer) {
	d, _ := json.MarshalIndent(arr, "", "  ")
	wto := fmt.Sprintf("%v", uuid.New())
	ioutil.WriteFile("/tmp/arr-"+wto+".json", d, os.ModePerm)
}
func main() {
	fmt.Println("Staring Media Orginaztion")
	srcFolder := config.GetSourceMediaFolder()
	destFolder := config.GetDestinationMediaFolder()
	arr := fs.WalkSource(srcFolder, destFolder)
	if len(arr) == 0 {
		fmt.Printf("\n\n\nNo NewMedia\n\n\n")
		return
	}
	fss := fs.ReadyFileTransfers(arr, destFolder)
	for _, v := range fss {
		fmt.Println("==== ", v.Title)
		fmt.Println("Old: ", v.OldDir)
		fmt.Println("New: ", v.NewDir)
	}

	if YesNoQuestion("Seems okay?") {
		fmt.Println("let the moving being!")
		fs.TransferFiles(fss)
	} else {
		fmt.Println("kay bye")
	}
}
