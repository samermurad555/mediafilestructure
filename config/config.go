package config

import (
	"os"
)

var jwttoken string

func envOrPanic(key string) string {
	if _env := os.Getenv(key); _env != "" {
		return _env
	}
	panic("Must Set " + key)
}
func SetToken(token string) {
	jwttoken = token
}

func GetToken() string {
	return jwttoken
}

func GetApiKey() string {
	return envOrPanic("TVDB_API_KEY")
}

func GetSourceMediaFolder() string {
	return envOrPanic("SRC_DIR")
}

func GetDestinationMediaFolder() string {
	return envOrPanic("DEST_DIR")
}
