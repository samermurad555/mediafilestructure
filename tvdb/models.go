package tvdb

import (
	"encoding/json"
	"net/http"

	"www.samermurad.com/mfsp/config"
)

type ApiResponseError string

func (aErr ApiResponseError) Error() string {
	return string(aErr)
}

type ApiResponse struct {
	Error ApiResponseError
	Body  map[string]interface{}
}

type ApiRequestHeaders map[string]string

type ApiRequest struct {
	Path    string
	Headers ApiRequestHeaders
	Method  string
	Body    []byte
}

func (req ApiRequest) SetHeaders(hdrs *http.Header) {
	for k, v := range req.Headers {
		hdrs.Set(k, v)
	}
}

func NewApiReq() ApiRequest {
	hdrs := make(ApiRequestHeaders)
	hdrs["Content-Type"] = "application/json"
	return ApiRequest{
		Method:  "GET",
		Headers: hdrs,
	}
}

func apiKeyBody() []byte {
	mp := make(map[string]string)
	mp["apikey"] = config.GetApiKey()
	b, _ := json.Marshal(mp)
	return b
}
