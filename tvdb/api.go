package tvdb

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

const BASE_URL = "https://api.thetvdb.com"

func apiRequest(req ApiRequest, ch chan *ApiResponse) {
	url := BASE_URL + string(req.Path)
	_req, err := http.NewRequest(req.Method, url, bytes.NewBuffer(req.Body))
	if err != nil {
		ch <- &ApiResponse{Error: ApiResponseError(err.Error())}
		return
	}
	req.SetHeaders(&_req.Header)
	fmt.Println("Executing: ", url, req.Method, string(req.Body))
	client := &http.Client{}
	response, err := client.Do(_req)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		ch <- &ApiResponse{Error: ApiResponseError(err.Error())}
	} else {
		data, _ := ioutil.ReadAll(response.Body)
		mp := make(map[string]interface{})
		err := json.Unmarshal(data, &mp)
		if err != nil {
			ch <- &ApiResponse{Error: ApiResponseError(err.Error())}
		} else {
			ch <- &ApiResponse{Body: mp}
		}
	}
}
func Login(ch chan *ApiResponse) {
	request := NewApiReq()
	request.Path = "/login"
	request.Method = "POST"
	request.Body = apiKeyBody()
	apiRequest(request, ch)
}

// func Search(chan )
